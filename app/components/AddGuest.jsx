var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var AddGuest = React.createClass({

    componentDidMount: function () {
        this.props.getData();
    },
    render: function () {
        if (this.props.data.fetching) {
            return <h5>Loading.....</h5>;
        }
        console.log('in render', this.props.data);
        if (!this.props.data.fetching) {
            var guestList = this.props.data.conference.guests;
            var heading = this.props.data.conference.conference;
            var host = this.props.data.conference.host;
            return (
                <div>
                    <div className="row">

                        <h3 className="text-center">{heading}</h3>
                    </div>

                    <h4 className="text-center">Host: {host}</h4>
                    <div className="col-xs-6 col-xs-offset-3">
                        <input className="form-control" ref="guestName" type="text" placeholder="your name"/>
                        <br/>
                        <button className="btn" onClick={() => this.props.setData(this.refs.guestName.value)}>Add me
                        </button>
                        <br/>
                        <br/>

                    </div>

                    <div className=" well col-xs-6 col-xs-offset-3">

                        <ol className="list-group">
                            {
                                Object.keys(guestList).map(function (key) {
                                    return <li className="list-group-item" key={key}>{guestList[key]}</li>;
                                })
                            }
                        </ol>
                    </div>
                </div>
            );

        }

    },

});

function mapStateToProps(state) {

    return {
        data: state.itemsReducer,
    };
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({
        setData: actions.setData,
        getData: actions.getObjectAction
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(AddGuest);