var firebase = require('firebase');

var config = {
    apiKey: "AIzaSyClVHsGDsv4-nOgRpuxaP4UnjvaGRwYxys",
    authDomain: "react-invite-application.firebaseapp.com",
    databaseURL: "https://react-invite-application.firebaseio.com",
    storageBucket: "react-invite-application.appspot.com",
    messagingSenderId: "669121562652"
};
firebase.initializeApp(config);

export var setData = function (name) {
    console.log("SomeThingClicke",name);
    var addGuest = firebase.database().ref('/guests');
    addGuest.push(name);
    return {
        type: "SAVE_DATA",
        guest: name,
    };
}

export var startFetching = function () {
    return {
        type: "STARTING_FETCHING",
    };
}

export var doneFetching = function (returnedObject) {
    return {
        type: "DONE_FETCHING",
        conference: returnedObject,
    };
}


export var getObjectAction = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());

        //yahn pr firebase call krunga

        var getItem = firebase.database().ref('/');
        getItem.on('value', function (snapshot) {
            dispatch(doneFetching(snapshot.val()));
        });
    };
};
