var Redux=require('redux');
var {itemsReducer}=require('./../reducers/index');
var thunk=require('redux-thunk').default;

export var storeConfig = function () {
    var reducer = Redux.combineReducers({
        itemsReducer: itemsReducer,
    });

    var store = Redux.createStore(reducer,Redux.applyMiddleware(thunk));

    return store;
}