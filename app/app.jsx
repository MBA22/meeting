var React = require('react');
var ReactDOM = require('react-dom');
var {Provider} = require('react-redux');
var Main = require('Main');
var actions=require('./actions/index');
var store=require('./store/configureStore').storeConfig();
store.dispatch(actions.getObjectAction());
ReactDOM.render(
    <Provider store={store}>
        <Main></Main>
    </Provider>,
    document.getElementById('app')
);

require('./redux-try.jsx');