export var itemsReducer = function (state = {}, action) {

    switch (action.type) {
        case 'STARTING_FETCHING':
            var newState = Object.assign({}, state, {fetching: true});
            return newState;

        case 'DONE_FETCHING': {
            var newState = Object.assign({}, state, {fetching: false, conference: action.conference});
            return newState;
        }
        case 'SAVE_DATA':
            var newState = Object.assign({}, state, {guests: action.guest});
            return newState;

        default:
            return state;
    }

};